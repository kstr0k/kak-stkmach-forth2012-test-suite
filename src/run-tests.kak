decl -hidden str stkmach_forth_tests_src %val{source}

def -override stkmach-forth-tests-run -params 1 %{
  decl -hidden str stkmach_forth_tests_dir %sh{
    set -- "${kak_opt_stkmach_forth_tests_src%/*}"; [ -n "$1" ] || set -- /
    printf %s "$1"
  }

  "%arg{1}-eval-forth" %{ : source pad 0 ; variable >in 0 >in ! }

  "%arg{1}-eval-forth-file" "%opt{stkmach_forth_tests_dir}/ttester.fs"
  "%arg{1}-eval-forth-file" "%opt{stkmach_forth_tests_dir}/core.fr"
  "%arg{1}-eval-forth-file" "%opt{stkmach_forth_tests_dir}/coreplustest.fth"
  "%arg{1}-eval-forth-file" "%opt{stkmach_forth_tests_dir}/coreexttest.fth"
  "%arg{1}-eval-forth-file" "%opt{stkmach_forth_tests_dir}/stringtest.fth"
}
