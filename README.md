# forth2012-test-suite for kak-stkmach

This is a version of the [Forth 2012 testsuite](https://github.com/gerryjackson/forth2012-test-suite) repository adapted for [`kak-stkmach`](https://gitlab.com/kstr0k/kak-stkmach/).

The [original README](doc/README.upstream.md) has been moved to [doc](doc).

## Usage

- clone this repo under your `kak-stkmach` installed folder
- call `stkmach-forth-tests-run ff` (or whatever you booted stkmach instance prefix is). Testing may take on the order of minutes so be prepared to wait. This command (which provides the exact list of tests that `kak-stkmach` is supposed to pass) is defined in [`src/run-tests.kak`](src/run-tests.kak)

*Tip*: `stkmach-kks-time-cmd-dbg stkmach-forth-tests-run ff` will print elapsed time at the end; it may also let `kak` provide some visual feedback while running, since it goes through a `command_fifo`.

## Changes

- `kak-stkmach` only has a Forth (approximate) tokenizer, not a parser. In particular, but not exclusively, `TESTING ...` has been changed to `.( TESTING ... )`.
- Skipped tests are prefixed with `\ #SKIP`
